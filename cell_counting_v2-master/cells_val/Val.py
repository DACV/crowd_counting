# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 19:12:53 2017

@author: Weidi Xie

@Description: This is the file used for training, loading images, annotation, training with model.
"""

import numpy as np
import os
from scipy import misc
from keras.models import load_model
from matplotlib import pyplot as plt


base_path = 'cells/'
data = []
anno = []

   
def read_data(base_path):
    imList = os.listdir(base_path)
    for i in range(len(imList)): 
        if 'cell' in imList[i]:
            img1 = misc.imread(os.path.join(base_path,imList[i]))
            print(img1)
            plt.imshow(img1)
            data.append(img1)
            
                        
    return np.asarray(data, dtype = 'float32')


def train_(base_path):
    
    data = read_data(base_path)
    #print(data)
    mean = np.mean(data)
    print(mean)
    std = np.std(data)
    print(std)
    data_ = (data - mean) / std
    val_data = data_[:]
    cnn = load_model('cell_counting.h5')
    A = cnn.predict(val_data)
    #print ('',A)
    mean_diff = np.average(np.abs(np.sum(np.sum(A,1),1)-np.sum(np.sum(val_data,1),1))) / (100.0)
    print('The counting people is : {} per image.'.format(np.abs(mean_diff)))
    
if __name__ == '__main__':
    train_(base_path)
