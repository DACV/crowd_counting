# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 13:00:58 2018

@author: Innervycs
"""

import cv2
import glob
import os

path = './'

for img in glob.glob(path + "*.jpg"):
    
   head, tail = os.path.split(img)
   name = os.path.splitext(tail)[0]
    
   img = cv2.imread(img)
   r_image = cv2.resize(img,(256,256))
   cv2.imwrite(name + '.png',r_image)