crowd_counting

Daniel Contreras, Valparaiso, 26 enero 2018
Basado en el trabajo realizado por Weidi Xie, J. Alison Noble, Andrew Zisserman en el trabajo "Microscopy Cell Counting with Fully 
Convolutional Regression Networks" se intentó adaptar un modelo de rede neuronal convolucional con upsampling para conteo de personas.

Para esto se creó una base de datos con características similares a las útilizada por ellos, a la cual se le aplicó marcación de personas
para obtener archivos dots, además de crear una base de datos de coordenadas para utilización futura.

Los scripts para el entrenamiento, creación del modelo y generación de aumentación de datos fueron:
-train.py
-model.py
-generator.py

estos son los scripts originales suministrados por Xie